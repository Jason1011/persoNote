--Lua 的表达式

print(true and false)
print(false or true)

print('hello'..' '..'world') --连接符号是..

function foo()
    local a = 1
end

foo()
print(a) -->>>>>>>>>>>>>nil 因为此时的local相当于是声明变量，local a 是局部变量，print (a) a 是全局变量

function bool()
    local a =1 
    return a +2
end
print(bool())