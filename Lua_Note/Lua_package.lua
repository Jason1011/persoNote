-- Lua 的包(package)

local c = require('foo')
print(c.foo(1,2))

for i = 1,2 do
    print(dofile('foo.lua'))
end

for i = 1,2 do
    print(require('foo'))
end

-- lua5.3 之后不再支持dostring