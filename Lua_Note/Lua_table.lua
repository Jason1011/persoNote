--lua的Table
--Table = 数组 + 映射

-- a = {}
-- a[1] = 10
-- a[2] = 20
-- a[3] = 'hello'

-- a = {
--     10,
--     20,
--     'hello'
-- }
-- print(a[1],a[2],a[3])

a = {}
a['hello'] = 2
a[3] = false

b={
    ['world']=3,
    [4]=true
}
print(a.hello,a[3],b.world,b[4])