-- Lua 的for泛型遍历
a = {
    ['foo'] = 1,
    [100] = true,
    ['love'] = 'submara',

    [3] = 30,
    [4] = 40,
}

for k,v in pairs(a) do
    print(k,v)
end

for k,v in ipairs(a) do
    print(k,v)
end

-- pairs() ipairs()  迭代器
